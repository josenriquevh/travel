Autor
===========

José Enrique Velásquez
  josenriquev@gmail.com


Instalación
===========

1. Copiar el proyecto dentro de una ruta del servidor web.

2. Importar la estructura de BD en MySQL a través del archivo data/agencia.sql
   NOTA: Puede modificar los valores de usuario y clave de BD de acuerdo a lo que tenga configurado en el servidor de base de datos.

3. Ajustar los parámetros de conexión a la BD en app/config/parameters.yml


URLs disponibles
================

NOTA: Cambiar "localhost" por el alias del dominio del servidor donde se aloja el API

http://localhost/travel/web/api/viajero
_______________________________________
* Funcionalidad: Permite la inclusión de los datos de un nuevo viajero.
* Método: POST
* Parámetros del body:
  * cedula
  * nombre
  * direccion
  * telefono

http://localhost/travel/web/api/actualizar_viajero
_______________________________________________________
* Funcionalidad: Permite la actualización de los datos de viajero existente.
* Método: POST
* Parámetros del body:
  * cedula
  * nombre
  * direccion
  * telefono

http://localhost/travel/web/api/viajeros
_____________________________________________
* Funcionalidad: Retorna el listado de todos los viajeros almacenados en BD.
* Método: GET

http://localhost/travel/web/api/viaje
__________________________________________
* Funcionalidad: Permite la inclusión de los datos de un nuevo viaje.
* Método: POST
* Parámetros del body: 
  * codigo
  * plazas
  * destino
  * origen
  * precio

http://localhost/travel/web/api/actualizar_viaje
_____________________________________________________
* Funcionalidad: Permite la actualización del número de plazas y el precio un viaje existente.
* Método: POST
* Parámetros del body: 
  * codigo
  * plazas
  * precio

http://localhost/travel/web/api/viajes
___________________________________________
* Funcionalidad: Retorna el listado de todos los viajes almacenados en BD.
* Método: GET

http://localhost/travel/web/api/tomar_viaje
________________________________________________
* Funcionalidad: Agenda el viaje para un viajero, pudiendo indicar la fecha de ida y la fecha de vuelt  
			   Cada viaje que se agenda resta el número de plazas para código del viaje.
* Método: POST
* Parámetros del body: 
  * cedula
  * codigo_viaje
  * fecha_ida
  * fecha_vuelta

http://localhost/travel/web/api/viajes_tomados
___________________________________________________
* Funcionalidad: Retorna el listado de todos los viajes agendados.
* Método: GET
