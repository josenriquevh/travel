<?php

namespace Strapp\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Viaje
 *
 * @ORM\Table(name="viaje")
 * @ORM\Entity
 */
class Viaje
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="codigo", type="integer", nullable=false)
     */
    private $codigo;

    /**
     * @var integer
     *
     * @ORM\Column(name="plazas", type="integer", nullable=false)
     */
    private $plazas;

    /**
     * @var string
     *
     * @ORM\Column(name="destino", type="string", length=100, nullable=false)
     */
    private $destino;

    /**
     * @var string
     *
     * @ORM\Column(name="origen", type="string", length=100, nullable=false)
     */
    private $origen;

    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $precio;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param integer $codigo
     *
     * @return Viaje
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return integer
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set plazas
     *
     * @param integer $plazas
     *
     * @return Viaje
     */
    public function setPlazas($plazas)
    {
        $this->plazas = $plazas;

        return $this;
    }

    /**
     * Get plazas
     *
     * @return integer
     */
    public function getPlazas()
    {
        return $this->plazas;
    }

    /**
     * Set destino
     *
     * @param string $destino
     *
     * @return Viaje
     */
    public function setDestino($destino)
    {
        $this->destino = $destino;

        return $this;
    }

    /**
     * Get destino
     *
     * @return string
     */
    public function getDestino()
    {
        return $this->destino;
    }

    /**
     * Set origen
     *
     * @param string $origen
     *
     * @return Viaje
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * Get origen
     *
     * @return string
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * Set precio
     *
     * @param string $precio
     *
     * @return Viaje
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string
     */
    public function getPrecio()
    {
        return $this->precio;
    }
}
