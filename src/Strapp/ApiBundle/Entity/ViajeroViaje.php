<?php

namespace Strapp\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ViajeroViaje
 *
 * @ORM\Table(name="viajero_viaje", indexes={@ORM\Index(name="viajero_id", columns={"viajero_id"}), @ORM\Index(name="viaje_id", columns={"viaje_id"})})
 * @ORM\Entity
 */
class ViajeroViaje
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="checkin", type="date", nullable=true)
     */
    private $checkin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="checkout", type="date", nullable=true)
     */
    private $checkout;

    /**
     * @var \Strapp\ApiBundle\Entity\Viaje
     *
     * @ORM\ManyToOne(targetEntity="Strapp\ApiBundle\Entity\Viaje")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="viaje_id", referencedColumnName="id")
     * })
     */
    private $viaje;

    /**
     * @var \Strapp\ApiBundle\Entity\Viajero
     *
     * @ORM\ManyToOne(targetEntity="Strapp\ApiBundle\Entity\Viajero")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="viajero_id", referencedColumnName="id")
     * })
     */
    private $viajero;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set checkin
     *
     * @param \DateTime $checkin
     *
     * @return ViajeroViaje
     */
    public function setCheckin($checkin)
    {
        $this->checkin = $checkin;

        return $this;
    }

    /**
     * Get checkin
     *
     * @return \DateTime
     */
    public function getCheckin()
    {
        return $this->checkin;
    }

    /**
     * Set checkout
     *
     * @param \DateTime $checkout
     *
     * @return ViajeroViaje
     */
    public function setCheckout($checkout)
    {
        $this->checkout = $checkout;

        return $this;
    }

    /**
     * Get checkout
     *
     * @return \DateTime
     */
    public function getCheckout()
    {
        return $this->checkout;
    }

    /**
     * Set viaje
     *
     * @param \Strapp\ApiBundle\Entity\Viaje $viaje
     *
     * @return ViajeroViaje
     */
    public function setViaje(\Strapp\ApiBundle\Entity\Viaje $viaje = null)
    {
        $this->viaje = $viaje;

        return $this;
    }

    /**
     * Get viaje
     *
     * @return \Strapp\ApiBundle\Entity\Viaje
     */
    public function getViaje()
    {
        return $this->viaje;
    }

    /**
     * Set viajero
     *
     * @param \Strapp\ApiBundle\Entity\Viajero $viajero
     *
     * @return ViajeroViaje
     */
    public function setViajero(\Strapp\ApiBundle\Entity\Viajero $viajero = null)
    {
        $this->viajero = $viajero;

        return $this;
    }

    /**
     * Get viajero
     *
     * @return \Strapp\ApiBundle\Entity\Viajero
     */
    public function getViajero()
    {
        return $this->viajero;
    }
}
