<?php

namespace Strapp\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Strapp\ApiBundle\Entity\Viajero;
use Strapp\ApiBundle\Entity\Viaje;
use Strapp\ApiBundle\Entity\ViajeroViaje;

class DefaultController extends Controller
{

	// Mensajes predefinidos
	const NO_PARAMETERS = "No están definidos todos los parámetros.";
	const NO_DATA = "Algunos datos son requeridos.";
	const BAD_DATA = "Error de validación de data.";
	const EXISTING_DATA = "Error de dato duplicado.";
	const NON_EXISTING_DATA = "Error de dato no encontrado.";
	const ZERO_DATA = "No hay disponibilidad.";

	// Función para devolver una petición incorrecta
	private function badRequest($msg, $help=null)
	{
		return array('message' => $msg,
					 'help' => $help);
	}

	// Función que serializa un objeto viajero
	private function serializeViajero(Viajero $viajero)
	{
		return array('cedula' => $viajero->getCedula(),
					 'nombre' => $viajero->getNombre(),
					 'direccion' => $viajero->getDireccion(),
					 'telefono' => $viajero->getTelefono());
	}

	// Función que valida los datos del POST del viajero
	private function validarViajero($request, $nuevo_viajero)
	{

		$em = $this->getDoctrine()->getManager();

		// Parámetros del POST
        $cedula = $request->request->get('cedula');
        $nombre = $request->request->get('nombre');
        $direccion = $request->request->get('direccion');
        $telefono = $request->request->get('telefono');

        $ok = 1;
        $error = array();

        if (!isset($cedula) || !isset($nombre) || !isset($direccion) || !isset($telefono))
        {
        	$ok = 0;
        	$error = $this->badRequest(self::NO_PARAMETERS, "Los parámetros para este request son: cedula, nombre, direccion, telefono.");
        }

        if ($ok && (!$cedula || !$nombre))
        {
        	$ok = 0;
        	$error = $this->badRequest(self::NO_DATA, "La cédula y el nombre del viajero son requeridos.");
        }

        if ($ok)
        {

        	if (!preg_match("/^[0-9]+$/", $cedula) || strlen($cedula) > 10)
	        {
	        	$ok = 0;
	        	$error = $this->badRequest(self::BAD_DATA, "La cédula debe ser numérica y no debe pasar de 10 dígitos.");
	        }
	        else {

	        	// Forzar a que el valor sea entero
	        	$cedula = intval($cedula);

	        	// Se valida que la cédula no exista
	        	$query = $em->createQuery('SELECT COUNT(v.id) FROM StrappApiBundle:Viajero v 
	                                        WHERE v.cedula = :cedula')
	                        ->setParameter('cedula', $cedula);
	            $existe_cedula = $query->getSingleScalarResult();

	            if ($nuevo_viajero)
	            {
	            	if ($existe_cedula)
		            {
		            	$ok = 0;
		            	$error = $this->badRequest(self::EXISTING_DATA, "Cédula ya existente.");
		            }
	            }
	            else {
	            	// Se valida que exista para poder actualizar
	            	if (!$existe_cedula)
		            {
		            	$ok = 0;
		            	$error = $this->badRequest(self::NON_EXISTING_DATA, "Cédula no existente.");
		            }
	            }

	        }

        }

        if ($ok && $telefono)
        {
        	if (strlen($telefono) > 15)
	        {
	        	$ok = 0;
	        	$error = $this->badRequest(self::BAD_DATA, "El teléfono puede ser máximo de 15 caracteres.");
	        }
        }

        return array('ok' => $ok,
        			 'error' => $error);

	}

	// Función que serializa un objeto viaje
	private function serializeViaje(Viaje $viaje)
	{
		return array('codigo' => $viaje->getCodigo(),
					 'plazas' => $viaje->getPlazas(),
					 'destino' => $viaje->getDestino(),
					 'origen' => $viaje->getOrigen(),
					 'precio' => $viaje->getPrecio());
	}

	// Función que valida los datos del POST del viaje
	private function validarViaje($request, $nuevo_viaje)
	{

		$em = $this->getDoctrine()->getManager();

		// Parámetros del POST
        $codigo = $request->request->get('codigo');
        $plazas = $request->request->get('plazas');
        $destino = $request->request->get('destino');
        $origen = $request->request->get('origen');
        $precio = $request->request->get('precio');

        $ok = 1;
        $error = array();

        if ($nuevo_viaje && (!isset($codigo) || !isset($plazas) || !isset($precio) || !isset($destino) || !isset($origen)))
        {
        	$ok = 0;
        	$error = $this->badRequest(self::NO_PARAMETERS, "Los parámetros para este request son: codigo, plazas, destino, origen, precio.");
        }

        if ($ok && !$nuevo_viaje && (!isset($codigo) || !isset($plazas) || !isset($precio)))
        {
        	$ok = 0;
        	$error = $this->badRequest(self::NO_PARAMETERS, "Los parámetros para este request son: codigo, plazas, precio.");
        }

        if ($ok)
        {
        	if ($nuevo_viaje && (!$codigo || !$plazas || !$destino || !$origen || !$precio))
        	{
        		$ok = 0;
        	}
        	elseif (!$nuevo_viaje && (!$codigo || !$plazas || !$precio)) {
        		$ok = 0;
        	}
        	if (!$ok)
        	{
        		$error = $this->badRequest(self::NO_DATA, "Todos los datos son requeridos.");
        	}
        }

        if ($ok)
        {

        	if (!preg_match("/^[0-9]+$/", $codigo) || strlen($codigo) > 10)
	        {
	        	$ok = 0;
	        	$error = $this->badRequest(self::BAD_DATA, "El código del viaje debe ser numérico y no debe pasar de 10 dígitos.");
	        }
	        else {

	        	// Forzar a que el valor sea entero
	        	$codigo = intval($codigo);

	        	// Se valida que el código no exista
	        	$query = $em->createQuery('SELECT COUNT(v.id) FROM StrappApiBundle:Viaje v 
	                                        WHERE v.codigo = :codigo')
	                        ->setParameter('codigo', $codigo);
	            $existe_codigo = $query->getSingleScalarResult();

	            if ($nuevo_viaje)
	            {
	            	if ($existe_codigo)
		            {
		            	$ok = 0;
		            	$error = $this->badRequest(self::EXISTING_DATA, "Código de viaje ya existente.");
		            }
	            }
	            else {
	            	// Se valida que exista para poder actualizar
	            	if (!$existe_codigo)
		            {
		            	$ok = 0;
		            	$error = $this->badRequest(self::NON_EXISTING_DATA, "Código de viaje no existente.");
		            }
	            }

	        }

        }

        if ($ok && (!preg_match("/^[0-9]+$/", $plazas) || strlen($plazas) > 10))
        {
        	$ok = 0;
        	$error = $this->badRequest(self::BAD_DATA, "El número de plazas debe ser numérico y no debe pasar de 10 dígitos.");
        }

        if ($ok && (!is_numeric($precio)))
        {
        	$ok = 0;
        	$error = $this->badRequest(self::BAD_DATA, "El precio debe ser numérico o decimal. ");
        }

        return array('ok' => $ok,
        			 'error' => $error);

	}

	// Función que valida una fecha en el formato especificado
	private function validateDate($date, $format = 'Y-m-d')
	{
	    $d = \DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) === $date;
	}

	// Función que valida que la fecha de inicio sea menor a la fecha final
	private function validateSchedule($date_end, $date_start = '')
	{
	    $date_start = $date_start != '' ? $date_start : date('Y-m-d');
	    return $date_end > $date_start;
	}

	// Función que serializa un objeto viajero_viaje
	private function serializeViajeroViaje(ViajeroViaje $viajero_viaje)
	{
		return array('cedula' => $viajero_viaje->getViajero()->getCedula(),
					 'nombre' => $viajero_viaje->getViajero()->getNombre(),
					 'codigo_viaje' => $viajero_viaje->getViaje()->getCodigo(),
					 'destino' => $viajero_viaje->getViaje()->getDestino(),
					 'origen' => $viajero_viaje->getViaje()->getOrigen(),
					 'fecha_ida' => $viajero_viaje->getCheckin()->format('Y-m-d'),
					 'fecha_vuelta' => $viajero_viaje->getCheckout()->format('Y-m-d'),
					 'precio' => $viajero_viaje->getViaje()->getPrecio());
	}

    public function ajaxViajeroAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();

        // Validar el POST
        $validacion = $this->validarViajero($request, 1);
        if (!$validacion['ok'])
        {
        	return new JsonResponse($validacion['error'], 400);
        }

        // Parámetros del POST
        $cedula = $request->request->get('cedula');
        $nombre = $request->request->get('nombre');
        $direccion = $request->request->get('direccion');
        $telefono = $request->request->get('telefono');

        // Forzar a que el valor sea entero
        $cedula = intval($cedula);

        $viajero = new Viajero();
        $viajero->setCedula($cedula);
        $viajero->setNombre($nombre);
        $viajero->setDireccion($direccion);
        $viajero->setTelefono($telefono);
        $em->persist($viajero);
        $em->flush();

        $data['viajero'][] = $this->serializeViajero($viajero);
                    
        return new JsonResponse($data, 200);
        
    }

    public function ajaxUpdateViajeroAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();

        // Validar el POST
        $validacion = $this->validarViajero($request, 0);
        if (!$validacion['ok'])
        {
        	return new JsonResponse($validacion['error'], 400);
        }

        // Parámetros del POST
        $cedula = $request->request->get('cedula');
        $nombre = $request->request->get('nombre');
        $direccion = $request->request->get('direccion');
        $telefono = $request->request->get('telefono');

        // Forzar a que el valor sea entero
        $cedula = intval($cedula);

        $viajero = $em->getRepository('StrappApiBundle:Viajero')->findOneByCedula($cedula);
        $viajero->setNombre($nombre);
        $viajero->setDireccion($direccion);
        $viajero->setTelefono($telefono);
        $em->persist($viajero);
        $em->flush();

        $data['viajero'][] = $this->serializeViajero($viajero);
                    
        return new JsonResponse($data, 200);
        
    }

    public function ajaxViajerosAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();

        $viajeros = $em->getRepository('StrappApiBundle:Viajero')->findAll();

        if (!$viajeros)
        {
        	$data['viajeros'][] = array();
        }
        else {
        	foreach ($viajeros as $viajero)
	        {
	        	$data['viajeros'][] = $this->serializeViajero($viajero);
	        }
        }
                    
        return new JsonResponse($data, 200);
        
    }

    public function ajaxViajeAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();

        // Validar el POST
        $validacion = $this->validarViaje($request, 1);
        if (!$validacion['ok'])
        {
        	return new JsonResponse($validacion['error'], 400);
        }

        // Parámetros del POST
        $codigo = $request->request->get('codigo');
        $plazas = $request->request->get('plazas');
        $destino = $request->request->get('destino');
        $origen = $request->request->get('origen');
        $precio = $request->request->get('precio');

        // Forzar a que los valores sea entero
        $codigo = intval($codigo);
        $plazas = intval($plazas);

        // Forzar a que el precio sea decimal
        $precio = (float)$precio;

        $viaje = new Viaje();
        $viaje->setCodigo($codigo);
        $viaje->setPlazas($plazas);
        $viaje->setDestino($destino);
        $viaje->setOrigen($origen);
        $viaje->setPrecio($precio);
        $em->persist($viaje);
        $em->flush();

        $data['viaje'][] = $this->serializeViaje($viaje);
                    
        return new JsonResponse($data, 200);
        
    }

    public function ajaxUpdateViajeAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();

        // Validar el POST
        $validacion = $this->validarViaje($request, 0);
        if (!$validacion['ok'])
        {
        	return new JsonResponse($validacion['error'], 400);
        }

        // Parámetros del POST
        $codigo = $request->request->get('codigo');
        $plazas = $request->request->get('plazas');
        $precio = $request->request->get('precio');

        // Forzar a que los valores sea entero
        $codigo = intval($codigo);
        $plazas = intval($plazas);

        // Forzar a que el precio sea decimal
        $precio = (float)$precio;

        $viaje = $em->getRepository('StrappApiBundle:Viaje')->findOneByCodigo($codigo);
        $viaje->setPlazas($plazas);
        $viaje->setPrecio($precio);
        $em->persist($viaje);
        $em->flush();

        $data['viaje'][] = $this->serializeViaje($viaje);
                    
        return new JsonResponse($data, 200);
        
    }

    public function ajaxViajesAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();

        $viajes = $em->getRepository('StrappApiBundle:Viaje')->findAll();

        if (!$viajes)
        {
        	$data['viajes'][] = array();
        }
        else {
        	foreach ($viajes as $viaje)
	        {
	        	$data['viajes'][] = $this->serializeViaje($viaje);
	        }
        }
                    
        return new JsonResponse($data, 200);
        
    }

    public function ajaxTomarViajeAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();

		// Parámetros del POST
		$cedula = $request->request->get('cedula');
        $codigo = $request->request->get('codigo_viaje');
        $checkin = $request->request->get('fecha_ida');
        $checkout = $request->request->get('fecha_vuelta');

        // Validación de datos

        if (!isset($codigo) || !isset($cedula) || !isset($checkin) || !isset($checkout))
        {
        	return new JsonResponse($this->badRequest(self::NO_PARAMETERS, "Los parámetros para este request son: cedula, codigo_viaje, fecha_ida, fecha_vuelta."), 400);
        }

        if (!$cedula || !$codigo || !$checkin || !$checkout)
        {
        	return new JsonResponse($this->badRequest(self::NO_DATA, "Todos los datos son requeridos."), 400);
        }

        if (!preg_match("/^[0-9]+$/", $cedula) || strlen($cedula) > 10)
        {
        	return new JsonResponse($this->badRequest(self::BAD_DATA, "La cédula debe ser numérica y no debe pasar de 10 dígitos."), 400);
        }
        else {

        	// Forzar a que el valor sea entero
        	$cedula = intval($cedula);

        	// Se valida que la cédula exista
        	$query = $em->createQuery('SELECT COUNT(v.id) FROM StrappApiBundle:Viajero v 
                                        WHERE v.cedula = :cedula')
                        ->setParameter('cedula', $cedula);
            $existe_cedula = $query->getSingleScalarResult();

            if (!$existe_cedula)
            {
            	return new JsonResponse($this->badRequest(self::NON_EXISTING_DATA, "Cédula no existente."), 400);
            }

        }

        if (!preg_match("/^[0-9]+$/", $codigo) || strlen($codigo) > 10)
        {
        	return new JsonResponse($this->badRequest(self::BAD_DATA, "El código del viaje debe ser numérico y no debe pasar de 10 dígitos."), 400);
        }
        else {

        	// Forzar a que el valor sea entero
        	$codigo = intval($codigo);

        	// Se valida que el código exista
        	$query = $em->createQuery('SELECT COUNT(v.id) FROM StrappApiBundle:Viaje v 
                                        WHERE v.codigo = :codigo')
                        ->setParameter('codigo', $codigo);
            $existe_codigo = $query->getSingleScalarResult();

            if (!$existe_codigo)
            {
            	return new JsonResponse($this->badRequest(self::NON_EXISTING_DATA, "Código del viaje no existente."), 400);
            }

        }

        if (!$this->validateDate($checkin))
        {
        	return new JsonResponse($this->badRequest(self::BAD_DATA, "Formato inválido de la fecha de ida. El formato correcto es: AAAA-MM-DD"), 400);
        }

        if (!$this->validateSchedule($checkin))
        {
        	return new JsonResponse($this->badRequest(self::BAD_DATA, "La fecha de ida debe ser futura."), 400);
        }

        if (!$this->validateDate($checkout))
        {
        	return new JsonResponse($this->badRequest(self::BAD_DATA, "Formato inválido de la fecha de vuelta. El formato correcto es: AAAA-MM-DD"), 400);
        }

        if (!$this->validateSchedule($checkout))
        {
        	return new JsonResponse($this->badRequest(self::BAD_DATA, "La fecha de vuelta debe ser futura."), 400);
        }

        if (!$this->validateSchedule($checkout, $checkin))
        {
        	return new JsonResponse($this->badRequest(self::BAD_DATA, "La fecha de ida debe ser menor a la fecha de vuelta."), 400);
        }

        $viajero = $em->getRepository('StrappApiBundle:Viajero')->findOneByCedula($cedula);
        $viaje = $em->getRepository('StrappApiBundle:Viaje')->findOneByCodigo($codigo);

        if ($viaje->getPlazas() < 1)
        {
        	return new JsonResponse($this->badRequest(self::ZERO_DATA, "Se ha agotado el número de plazas para este código de viaje."), 400);
        }

        // Almacenamiento en BD
        $viajero_viaje = new ViajeroViaje();
        $viajero_viaje->setViajero($viajero);
        $viajero_viaje->setViaje($viaje);
        $viajero_viaje->setCheckin(new \DateTime($checkin));
        $viajero_viaje->setCheckout(new \DateTime($checkout));
        $em->persist($viajero_viaje);
        $em->flush();

        // Restamos una plaza
        $plazas = $viaje->getPlazas() - 1;
        $viaje->setPlazas($plazas);
        $em->persist($viaje);
        $em->flush();

        $data['viaje_tomado'][] = $this->serializeViajeroViaje($viajero_viaje);
                    
        return new JsonResponse($data, 200);
        
    }

    public function ajaxViajesTomadosAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();

        $viajeros_viajes = $em->getRepository('StrappApiBundle:ViajeroViaje')->findAll();

        if (!$viajeros_viajes)
        {
        	$data['viajes_tomados'][] = array();
        }
        else {
        	foreach ($viajeros_viajes as $viajero_viaje)
	        {
	        	$data['viajes_tomados'][] = $this->serializeViajeroViaje($viajero_viaje);
	        }
        }
                    
        return new JsonResponse($data, 200);
        
    }

}
